var express = require('express');
var router = express.Router();

const Gdax = require('gdax');
const publicClient = new Gdax.PublicClient();

// irrelevant to the project
//https://medium.com/dailyjs/rewriting-javascript-converting-an-array-of-objects-to-an-object-ec579cafbfc7
const arrayToObject = (arr, keyField) =>
  Object.assign({}, ...arr.map(item => ({[item[keyField]]: item})))

function getProductsByCurrency(products, currency) {
    return new Promise((resolve, reject) => {

        let results = products.filter((product) => product.quote_currency == currency);
        if (results.length <= 0) {
            reject('No trading pair exists for currency specified');
        } else {
            console.log(results);
            resolve(results);
        }
    });
}

/* GET home page. */
router.get('/', function(req, res, next) {
    let templateOpts = new Object();

    publicClient
        .getProducts()
        .then((products) => {
            templateOpts.products = products;
            return getProductsByCurrency(products, 'USD');
        })
        .then((products) => {
            templateOpts.filteredProducts = products;

            res.render('index', templateOpts);
        })
        .catch((error) => {
            console.log(`There was an error: ${error}`)
            next(error);
        });
});

router.get('/', (req, res, next) => {

})

module.exports = router;

// userPromise.then(getPostsPromise).then(getThreadsPromise).then(initializeComplete);