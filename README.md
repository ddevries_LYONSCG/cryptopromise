# Crypto Promise

___

## An Express.js App Exploring ES6 Promises

### This app uses GDAX's API, Coinbase's official cryptocurrency exchange, to provide example in interacting with promises
```
git clone https://ddevries_LYONSCG@bitbucket.org/ddevries_LYONSCG/cryptopromise.git
cd cryptopromise
npm init
npm start

```

___

## Resources

Here's a list of resources I referenced to put this project together:

* [Let's Learn ES6 Promises (VIDEO)](https://youtu.be/vQ3MoXnKfuQ)
* [Callbacks](https://javascript.info/callbacks)
* [Promise Basics](https://javascript.info/promise-basics)
* [Promise Chaining](https://javascript.info/promise-chaining)
* [The Promise API](https://javascript.info/promise-api)
* [Async-Await](https://javascript.info/async-await)
* [Promise Patterns & Anti-Patterns](http://www.datchley.name/promise-patterns-anti-patterns/)
* [Promises Broken Down (Quick Reference)](https://promisesaplus.com/)

___

## Let's Get This Crypto $$$$

## Setup

I used the [Express Generator](https://www.npmjs.com/package/express-generator) to quickly set-up the project

```
npm install -g express-generator
express --ejs --view ejs --git [projectName]
cd [projectName]
npm install
npm start

```

After you have done any additional configuration that may be needed, let's set up our app to integrate with GDAX. For reference, here's the [GDAX Node API](https://github.com/coinbase/gdax-node) and [GDAX's offical documentation](https://docs.gdax.com/)

Run `npm install gdax`

Add the following code at the top of your index.js file to initialize the GDAX client object.

```javascript
const Gdax = require('gdax');
const publicClient = new Gdax.PublicClient();
```

## Our First Promise

*Note that I'm going to leave all client-side javascript, formatting, and styling out of this guide for the application. How you implement the format of data on the client-side is up to you.*

Inside our GET '/' route handler, lets create our first promise. GDAX is promising that they will send all their products'/cryptocurrencies' infomation at some time in the future with a this method call on the client object.

```javascript
let myFirstPromise = publicClient.getProducts();
```

Great, now we have recieved our first promise from the people at GDAX. We can think of the promise as *pending*.

If we recieve a good response from their server, **THEN** we get to decide what to do with the *fulfilled* promise and it's associated data. Else, we will **CATCH** the bad response as an error and handle the *rejected* promise :(

So let's do this. The code reads fairly straightforward.

```javascript
myFirstPromise.then((data) => {
    // the promise was resolved by the server
    // it is in the fulfilled state and has data from the server associated wit it
    // do things with data i.e render a web page adding the data recieved
})
.catch((error) => {
    // else, the promise was rejected by the server
    // it is in the rejected state and you need to decide the next action
    next(error); // passes the error to the 'catch all' error handler in app.js
})
```

Now let's put condense the code above, take the data from the fulfilled promise, and render a dynamic HTML page with the use of EJS, the templating engine. The entire code snippet of the route looks like this so far.

```javascript
router.get('/', function(req, res, next) {

    publicClient
    .getProducts()
    .then((data) => {
        res.render('index', data);
    })
    .catch((error) => {
         console.log(`There was an error: ${err}`)
         next(error);
    });

});
```

This works and we are getting a lot of data on cryptocurrency trading pairs

The API call to GDAX is returning a promise and often times this could be the extent to which you interact with a promise. You get one from an external service and then handle the promise depending on its state. But taking a step back, how do we actually construct our own promise. What if we want to make promise to our own application internally or respond to an external call to our server?








